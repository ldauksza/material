class App < Hyperloop::Router
  history :browser

  route do
    DIV do
      Roost::NavOverlay() if AppStore.app_menu_visible
      Roost::AppBar()
      DIV(id: :app) do

        # App Menu. ToDo: Move to it's own component but need to figure out how router works to
        # properly inherit props
        DIV(class: "app-menu #{menu_visible?}") do
          DIV(class: 'app-menu__content') do
            DIV(class: 'nav-buttons') do
              link_item destination: '/inbox',  label: 'Inbox',   icon: 'inbox'
              link_item destination: '/star',   label: 'Star',    icon: 'star'
              link_item destination: '/trash',  label: 'Trash',   icon: 'delete'
            end
          end
        end

        Switch do
          Route('/', exact: true) { Redirect('/inbox') }
          Route('/inbox', exact: true, mounts: Pages::Inbox)
          Route('/star', exact: true, mounts: Pages::Star)
          Route('/trash', exact: true, mounts: Pages::Trash)
        end
      end
    end
  end

  def menu_visible?
    'is-open' if AppStore.app_menu_visible
  end

  def link_item(destination:, label:, icon:)
    NavLink(destination, class: 'nav-buttons__button subhead-2', active_class: 'is-activated') do
      I(class: 'nav-buttons__icon material-icons') {icon} 
      label
    end
  end
end
