module Pages
  class Inbox < Hyperloop::Router::Component

    before_mount { AppStore.set_app_bar! title: 'Inbox' }
    
    before_mount do
      @form = Roost::Form::FormObject
      @form.set_model! Customer.find(1)

      @form.field :first_name, validate: {required: true}
      @form.field :last_name
      @form.field :company
    end

    render do 
      Roost::Grid() do
        Roost::FAB(action: fire)
        Roost::Card(screen: [12, 12, 12]) do
          Roost::Grid() do
            Roost::Form::TextField(name: :first_name, hint: 'This is a required field')
            Roost::Form::TextField(name: :last_name)
            Roost::Form::TextField(name: :company)
            #Roost::Form::TextField(name: :age, type: :number)
            #Roost::Form::TextField(name: :no_db, unbound: true)
            #Roost::Form::TextField(name: :password, type: :password, unbound: true)
            Roost::Form::Field(screen: [12, 6, 6]) do
              #Roost::Form::CheckBox(name: :rocket, label: 'Order Rocket')
              #Roost::Form::CheckBox(name: :order)
              #Roost::Form::CheckBox(name: :fix)
            end
            Roost::Form::Field(screen: [12, 12, 12], type: :right) do
              Roost::Form::Button(label: 'Save Customer', action: save)
            end
          end
          #P {"First Namve Validations: #{Roost::Form::FormObject.field(:first_name).validations}"}
          P {"Fields: #{Roost::Form::FormObject.fields}"}
          P {"First Name: #{Roost::Form::FormObject.fields[:first_name].value}"}
          P {"Model: #{Roost::Form::FormObject.model}"}
  
          #BUTTON() {'Validate'}.on(:click) { FormStore.validate! }
          
        end
      end
    end

    def save
      -> () { SaveFormOp.run }
    end

    def validate_name
      {
        required: true,
        min: 3, 
        max: 25
      }
    end

    def fire
      ->() { alert 'Fire!!!!' }
    end

  end
end
