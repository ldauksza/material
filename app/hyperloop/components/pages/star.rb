module Pages
  class Star < Hyperloop::Router::Component
    before_mount { AppStore.set_app_bar! title: 'Star Page' }

    render do 
      Roost::Grid() do
        Roost::Card(screen: [12, 12, 12]) do
          ShowRecords()
        end
      end
    end
  end
end
