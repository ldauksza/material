module Roost
  class AppBar < Hyperloop::Component
    render(HEADER, class: 'app-bar') do
      DIV(class: 'app-bar__menu material-icons') do
        :menu
      end.on(:click) do
        AppStore.toggle_app_menu
      end

      DIV(class: 'app-bar__title-area') do
        SPAN(class: 'title app-bar__title') { AppStore.app_bar[:title].to_s }
      end

      DIV(class: 'app-bar__nav-button material-icons') do
        'search'
      end

      DIV(class: 'app-bar__nav-button material-icons') do
        'more_vert'
      end
    end
  end
end
