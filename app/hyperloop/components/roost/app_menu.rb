module Roost
  class AppMenu

    def self.render_menu
      DIV(class: "app-menu #{menu_visible?}") do
        DIV(class: 'app-menu__content') do
          DIV(class: 'nav-buttons') do
            link_item destination: '/inbox',  label: 'Inbox',   icon: 'inbox'
            link_item destination: '/star',   label: 'Star',    icon: 'star'
            link_item destination: '/trash',  label: 'Trash',   icon: 'delete'
          end
        end
      end
    end

    def link_item(destination:, label:, icon:)
      NavLink(destination, class: 'nav-buttons__button subhead-2', active_class: 'is-activated',
              active: ->(match, location) { `#{location}.pathname` =~ /#{destination}/}) do
        I(class: 'nav-buttons__icon material-icons') {icon} 
        label
      end
    end

    def menu_visible?
      'is-open' if AppStore.app_menu_visible
    end
  end
end
