module Roost
  class Card < Hyperloop::Component
    param :screen, type: Array

    render do
      DIV(class: "card #{screen}") do
        children.each do |child|
          child.render
        end
      end
    end

    def screen
      "phone-#{params.screen[0]} tablet-#{params.screen[1]} desktop-#{params.screen[2]}"
    end

  end
end
