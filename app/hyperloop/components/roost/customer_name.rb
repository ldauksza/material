module Roost
  class CustomerName < Hyperloop::Component
    before_mount do
      @customer = Customer.find(1)
      force_update!
    end
    
    def render
      H1 { @customer.first_name }
    end
  end
end
