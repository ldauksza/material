module Roost
  class FAB < Hyperloop::Component
    param icon: :add
    param action: nil, type: Proc, allow_nil: true

    render do
      BUTTON class: 'fab material-icons' do
        SPAN class: 'fab__icon' do
          params.icon
        end
      end.on(:click) do
        params.action
      end
    end
  end
end
