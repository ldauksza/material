module Roost
  module Form
    class Button < Hyperloop::Component
      param :label
      param type: :raised #flat/raised/disabled
      param color: :primary #primary/secondary
      param disabled: false
      param :action, type: Proc

      render do
        BUTTON(class: "button #{type_and_color}") do
          params.label
        end
        .on(:click) do
          params.action
        end
      end

      def type_and_color
        if params.type == :flat
          "button__flat--#{params.color.to_s}"
        else
          "button__raised--#{params.color.to_s}"
        end
      end

    end
  end
end
