module Roost
  module Form
    class CheckBox < Hyperloop::Component
      param :name
      param checked: nil
      param label: nil
      
      state :checked

      before_mount do
        FormStore.f.load do
          @value = FormStore.f[params.name]
        end.then do |value|
          @value = value
        end

        @value.nil? ? mutate.checked(false) : mutate.checked(@value)
      end

      render do
        DIV(class: :checkbox) do
          INPUT(type: :checkbox, id: params.name, checked: state.checked)
          .on(:change) do
            mutate.checked !state.checked
            FormStore.mutate.f[params.name] = state.checked
          end

          LABEL(htmlFor: params.name) { params.label }
        end
      end
    end
  end
end
