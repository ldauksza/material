module Roost
  module Form
    class Field < Hyperloop::Component
      param :screen, type: Array
      param type: :inline #inline|right

      render do
        DIV(class: "#{type} #{screen}") do
          children.each do |child|
            child.render
          end
        end
      end

      def screen
        "phone-#{params.screen[0]} tablet-#{params.screen[1]} desktop-#{params.screen[2]}"
      end

      def type
        return 'form-field--inline' if params.type == :inline
        return 'form-field--right'  if params.type == :right
      end

    end
  end
end
