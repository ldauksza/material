module Roost
  module Form
    class TextField < Hyperloop::Component

      param screen: [12, 6, 6]
      param :name
      param label: nil
      param value: nil
      param hint: nil
      param unbound: false
      param type: :text

      state active: false
      state error: false

      before_mount do
        @errors = []
        @name = params.name
        @value = Roost::Form::FormObject.fields[@name].value
        set_label
        set_active?
      end

      render do
        DIV(class: "text-field #{screen}") do

          INPUT(type: params.type, id: @name, value: @value, class: "text-field__input #{error_class?}") do
          end
            .on(:change) do |e|
              @value = e.target.value
              set_active?
          end
            .on(:blur) do |e|
              @value = e.target.value
              Roost::Form::FormObject.update_value! name: @name, value: @value

              validate!
          end

          LABEL(class: "text-field__label #{active_class?} #{error_class?}") { @label }
          @errors.each do |error|
            SPAN(class: 'text-field__error') { error }
          end
          SPAN(class: 'text-field__hint') {params.hint} if params.hint
        end

      end

      #def should_component_update?
        #super || Roost::Form::FormObject.fields[@name].value != @initial_value
      #end

      def validate!
        if Roost::Form::FormObject.fields[@name].error?
          mutate.error true
        end
      end

      def screen
        "phone-#{params.screen[0]} tablet-#{params.screen[1]} desktop-#{params.screen[2]}"
      end

      def active_class?
        'is-active' if state.active
      end

      def active?
        state.active
      end

      def set_active?
        if @value.to_s.empty?
          mutate.active false
        else
          mutate.active true
        end
      end

      def error?
        state.error
      end

      def error_class?
        'is-error' if error?
      end

      def set_label
        if params.label.nil?
          @label = @name.to_s.split('_').map(&:capitalize).join(' ')
        else
          @label = params.label
        end
        
        @label = "#{@label}*" if Roost::Form::FormObject.fields[@name].required?
      end

    end
  end
end
