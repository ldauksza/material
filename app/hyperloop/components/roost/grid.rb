module Roost
  class Grid < Hyperloop::Component
    render do
      DIV(class: :grid) do
        children.each do |child|
          child.render
        end
      end
    end
  end
end
