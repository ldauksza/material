module Roost
  class NavOverlay < Hyperloop::Component

    render do
      DIV(class: 'nav-overlay') do
      end.on(:click) do
        AppStore.toggle_app_menu
      end
    end
  end
end
