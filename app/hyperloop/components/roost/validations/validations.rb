module Roost::Validations
  def validate_required
    @errors << 'This is required field' if @value.empty?
  end

  def validate_min_length(n)
    @errors << "Value needs to be at least #{n} letters long" if @value.length < n
  end

  def validate_max_length(n)
    @errors << "Value needs to be less than #{n} letters long" if @value.length > n
  end
end
