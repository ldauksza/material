module Roost
  class Wrap < Hyperloop::Component
    param :text

    render DIV do
      children.each do |child|
        child.render
      end
    end
  end
end
