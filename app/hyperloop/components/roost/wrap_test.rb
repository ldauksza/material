module Roost
  class WrapTest < Hyperloop::Component
    param :text

    render do
      P {params.text}
    end
  end
end
