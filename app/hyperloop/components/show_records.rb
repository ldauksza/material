class ShowRecords < Hyperloop::Component
  before_mount do
    @customers = Customer.all!
  end
  render DIV do
    TABLE do
      THEAD do
        TR do
          TH { 'First Name '}
          TH { 'Last Name '}
          TH { 'Company'}
          TH { 'Phone'}
          TH { 'Age'}
          TH { 'Rocket?'}
        end
      end

      TBODY do
        @customers.each do |customer|
          TR do
            TD {customer.first_name}
            TD {customer.last_name}
            TD {customer.company}
            TD {customer.phone}
            TD {customer.age.to_s}
            TD {customer.rocket.to_s}
          end
        end
      end
    end
    P { "Count: #{Customer.count}"}
  end
end
