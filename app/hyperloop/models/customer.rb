# == Schema Information
#
# Table name: customers
#
#  id         :integer          not null, primary key
#  first_name :string
#  last_name  :string
#  company    :string
#  phone      :string
#  age        :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  rocket     :boolean
#

class Customer < ApplicationRecord
  regulate_scope all: :always_allow

  validates :first_name, presence: true
end
