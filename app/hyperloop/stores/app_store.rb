class AppStore < Hyperloop::Store
  state app_bar: Hash.new, scope: :class, reader: true
  state app_menu_visible: true, scope: :class, reader: true

  def self.toggle_app_menu
    mutate.app_menu_visible !app_menu_visible
  end

  def self.set_app_bar!(**settings)
    settings.each do |key, value|
      mutate.app_bar[key] = value
    end
  end
end
