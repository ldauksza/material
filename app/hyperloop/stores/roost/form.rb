module Roost
  module Form
    class FormObject < Hyperloop::Store
      include Roost::Validations

      state model: nil, scope: :class, reader: true
      state fields: Hash.new, scope: :class, reader: true

      def self.set_model! obj
        mutate.model obj
      end

      #Create/Update FormField Objects
      def self.field(name, **args)
        value = state.model[name]

        if state.fields.key? name
          mutate.fields[name].value = value
        else
          mutate.fields[name] = FormField.new(
            name: name,
            value: value,
            validate: args[:validate]
          ) 
        end
      end

      def self.update_value!(name:, value:)
        mutate.fields[name].value = value
        #validate if validation is required
        if state.fields[name].validate?
          state.fields[name].validate!
        end
      end
    end

    class FormField
      attr_accessor :value
      attr_reader :name, :validate, :errors

      def initialize(name:, value:, validate:)
        @name = name
        @validate = validate
        @errors = []
        @value = value
      end

      def validate?
        !@validate.nil?
      end

      def required?
        validate? && validate[:required]
      end

      def validate!
        validate_required if required?
      end

      def error?
        @errors.any?
      end

      #Validation Functions
      #TODO: Move to seperate module
      def validate_required
        @errors << 'This is required field' if @value.empty?
      end

      def validate_min_length(n)
        @errors << "Value needs to be at least #{n} letters long" if @value.length < n
      end

      def validate_max_length(n)
        @errors << "Value needs to be less than #{n} letters long" if @value.length > n
      end
    end

  end
end
