Rails.application.routes.draw do
  #devise_for :users
  mount Hyperloop::Engine => '/hyperloop'

  root 'hyperloop#app'
  match '*all', to: 'hyperloop#app', via: [:get]

end
