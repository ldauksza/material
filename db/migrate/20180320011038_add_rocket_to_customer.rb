class AddRocketToCustomer < ActiveRecord::Migration[5.1]
  def change
    add_column :customers, :rocket, :boolean
  end
end
